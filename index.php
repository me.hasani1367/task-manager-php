<?php
$task = json_decode(file_get_contents('C:\\xampp\\htdocs\\php3.exp\\task\\db\\task.json'));
?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
  <meta charset="UTF-8">
  <title>وظایف</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="bg-light">
  <div class="container">
    <div class="text-center mt-5 mb-5">
      <h1>یاداوری وظایف</h1>
    </div>

    <div class="row">
      <div class="col-md-4 order-md-1">
        <h4 class="mb-3">افزودن وظایف</h4>
        <form action="panel\process\save-task.php" method="GET" class="needs-validation" novalidate>
          <div class="row">
            <div class="col-md-5 mb-3">
              <label for="country">روز</label>
              <select class="custom-select d-block w-100" name="day" id="country" required>
                <option value="">روز...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
              </select>
              <div class="invalid-feedback">
                Please select a valid country.
              </div>
            </div>
            <div class="col-md-4 mb-3">
              <label for="state">ساعت</label>
              <select class="custom-select d-block w-100" name="hour" id="state" required>
                <option value="">ساعت...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
              <div class="invalid-feedback">
                Please provide a valid state.
              </div>
            </div>
            <div class="col-md-3 mb-3">
              <label for="zip">دقیقه</label>
              <input type="number" name="min" class="form-control" id="zip" placeholder="" required>
              <div class="invalid-feedback">
                Zip code required.
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="address">عنوان وظیفه</label>
            <input type="text" name="content" class="form-control" id="address" placeholder="کد زدن" required>
            <div class="invalid-feedback">
              Please enter your shipping address.
            </div>
          </div>
          <button class="btn btn-primary btn-lg btn-block mt-4" type="submit">ثبت کردن وظیفه جدید</button>
        </form>
      </div>
      <div class="col-md-8 order-md-2 mb-4 border">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-muted mt-3">وظایف شما</span>
          <span class="badge badge-secondary badge-pill">3</span>
        </h4>
        <?php foreach ($task as $key => $value) : ?>
          <ul class="list-group mb-3 pr-0">
            <li class="list-group-item">
              <div>
                <strong><?= $value->content ?> </strong>
                <small class="text-muted"> در روز <?= $value->day ?> اوم ماه، ساعت <?= $value->hour ?> و <?= $value->min ?> دقیقه </small>
              </div>
              <a href="edit.php?key=<?= $key ?>"><button type="button" class="btn btn-success float-left">ویرایش</button></a>
              <a href="delete.php?key=<?= $key ?>"><button type="button" class="btn btn-danger float-left ml-1">حذف</button></a>             
            </li>
          </ul>
        <?php endforeach ?>
      </div>
    </div>
  </div>

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>